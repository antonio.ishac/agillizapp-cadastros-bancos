package br.com.agillizapp.cadastros.domains.services.mappers;

import br.com.agillizapp.cadastros.api.request.BankRequest;
import br.com.agillizapp.cadastros.domains.repositories.entities.BankEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface BankRequestMapper extends ConverterMapper<BankRequest, BankEntity> {
}
