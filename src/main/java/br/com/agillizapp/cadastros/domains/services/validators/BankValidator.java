package br.com.agillizapp.cadastros.domains.services.validators;

import br.com.agillizapp.cadastros.domains.config.handler.exception.ApiException;
import br.com.agillizapp.cadastros.domains.config.handler.util.ExceptionCodeEnum;
import br.com.agillizapp.cadastros.domains.services.dtos.BankDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class BankValidator {

    public void existsCodeBank(Boolean exists) {
        if (exists) {
            throw new ApiException(ExceptionCodeEnum.ERROR_NEW_BANK_CODE_EXISTS);
        }
    }

}
