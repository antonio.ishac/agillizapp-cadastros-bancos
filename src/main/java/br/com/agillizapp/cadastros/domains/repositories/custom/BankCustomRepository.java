package br.com.agillizapp.cadastros.domains.repositories.custom;

import br.com.agillizapp.cadastros.domains.repositories.entities.BankEntity;
import br.com.agillizapp.cadastros.domains.services.filter.BankFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface BankCustomRepository {
    Page<BankEntity> findByBankFilter(BankFilter filter, Pageable sortedByName );
}
