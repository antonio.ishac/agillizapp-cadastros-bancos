package br.com.agillizapp.cadastros.domains.services.mappers;

import br.com.agillizapp.cadastros.domains.repositories.entities.BankEntity;
import br.com.agillizapp.cadastros.domains.services.dtos.BankDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface BankMapper extends ConverterMapper<BankDTO, BankEntity> {
}
