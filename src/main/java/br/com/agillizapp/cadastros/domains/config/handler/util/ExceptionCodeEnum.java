package br.com.agillizapp.cadastros.domains.config.handler.util;

import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;

@Getter
public enum ExceptionCodeEnum {

    INTERNAL_SERVER_ERROR(HttpStatus.INTERNAL_SERVER_ERROR),
    ERROR_NEW_BANK_CODE_EXISTS(HttpStatus.BAD_REQUEST),
    ERROR_ID_BANK_NOT_FOUND(HttpStatus.BAD_REQUEST);

    private final HttpStatus httpStatus;

    ExceptionCodeEnum(final HttpStatus httpStatus ) {
        this.httpStatus = httpStatus;
    }

    public String getCode() {
        return this.name();
    }

}

