package br.com.agillizapp.cadastros.domains.services.utils;

import org.springframework.core.GenericTypeResolver;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.util.Map;

@Component
public class PatchService<T> {

    private final Class<T> genericType;

    public PatchService() {
        this.genericType = (Class<T>) GenericTypeResolver
                .resolveTypeArgument(getClass(), PatchService.class);
    }

    public T updateMergeFields(Map<String, Object> fields, Object objectOrigin, Object objectDestiny) {
        fields.forEach((nameProperty, value) -> {
            Field field = ReflectionUtils.findField(getClass(), nameProperty);
            field.setAccessible(true);

            Object newValue = ReflectionUtils.getField(field, objectOrigin);

            ReflectionUtils.setField(field, objectDestiny, newValue);
        });

        return (T) objectDestiny;
    }
}
